let randomDate = (start, end) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

let updateDate = () => {
  if (Math.random() < 0.4) { 
    modalTinyBtn.open();
    let modalText = modalContent[Math.floor(Math.random() * modalContent.length)];
    console.log(`modalText set to ${modalText}`);
    modalTinyBtn.setContent(modalText);
  }
  const elementDay = document.getElementById("day");
  const elementMonth = document.getElementById("month");
  const elementYear = document.getElementById("year");
  elementDay.style.opacity = '0';
  elementMonth.style.opacity = '0';
  elementYear.style.opacity = '0';

  const myRandomDate = randomDate(new Date(), new Date(2021, 4, 30))
  let strRandomDate = myRandomDate.toISOString().slice(0,10);
  console.log(strRandomDate);
  let formatter = new Intl.DateTimeFormat('en', { month: 'long' });
  let randomMonth = formatter.format(myRandomDate);
  let randomDay = myRandomDate.getDate()

  window.setTimeout(() => {
    elementDay.style.opacity = '1';
    elementMonth.style.opacity = '1';
    elementYear.style.opacity = '1';
    document.getElementById("month").innerHTML = randomMonth;
    document.getElementById("day").innerHTML = randomDay;

    updateChart(myRandomDate);
  }, 1000);

};

document.getElementById("button").addEventListener("click", updateDate);
document.getElementById("day").addEventListener("click", updateDate);


let modalTinyBtn = new tingle.modal({
    footer: true
});

let modalContent = [
    `<p>Congratulations, you've been selected for the QRF 5.0!</p>
<div style="width:100%;height:0;padding-bottom:48%;position:relative;"><iframe src="https://giphy.com/embed/tMUMi4Ylnzk88" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/tMUMi4Ylnzk88">via GIPHY</a></p>
`,
`AROWS is down, release date may not be accurate`,
`We are having a training day today, please come back tomorrow`,
`Congratulations, you have been selected for a QRF Stat Tour, acceptance is not necessary`,
`Congratulations, you have been issued morale.`,
`We are calling to issue you a final notice about your car's extended warranty. <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Follow this link to address this final notice.</a>`,
`<p>For your outstanding service to America, you've been selected for permanent detail to the Army. </p><div style="width:100%;height:0;padding-bottom:61%;position:relative;"><iframe src="https://giphy.com/embed/kRkJXYahXjSE0" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/marveledit-evansedit-capedit-kRkJXYahXjSE0">via GIPHY</a></p>`,
`<p>Due to budgetary constraints, the District of Columbia National Guard (brought to you by DeWALT Tools) has issued new uniforms. Proceed to LRS for pickup.</p><div style="width:100%;height:0;padding-bottom:100%;position:relative;"><iframe src="https://giphy.com/embed/l378pU1T75lUZYkRG" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/nascar-matt-kenseth-camry-crew-l378pU1T75lUZYkRG">via GIPHY</a></p>`,
`<p>Do you like tapes and CDs? Well you can see</p>
<div style="width:100%;height:0;padding-bottom:100%;position:relative;"><iframe src="https://giphy.com/embed/l0Exh5setxQl10lUI" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/food-vegan-nuts-l0Exh5setxQl10lUI">via GIPHY</a></p>
`
];

modalTinyBtn.setContent(modalContent[Math.floor(Math.random() * modalContent.length)]);
modalTinyBtn.addFooterBtn('We have blamed the CF for this', 'tingle-btn tingle-btn--danger', function(){
    // alert('click on danger button!');
    modalTinyBtn.close();
});

function updateChart(mrd) {

    var datesServed = parseInt((new Date() - new Date(2021,0,6))/(24*3600*1000));
    var datesRemaining = parseInt((mrd - new Date())/(24*3600*1000));
    ;
    let formatter = new Intl.DateTimeFormat('en', { month: 'long' });
    let randomMonth = formatter.format(mrd);
    let randomDay = mrd.getDate()

    CanvasJS.addColorSet("customColorSet1",
        [//colorSet Array
            "#f1f1f1",
            "#00308f",
        ]);

    var chart = new CanvasJS.Chart("chartContainer", {
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        colorSet:  "customColorSet1",
        title: {
            text: "Time until release"
        },
        data: [{
            type: "doughnut",
            startAngle: 90,
            toolTipContent: "<b>{label}</b>: {y}",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{label}: {y}",
            dataPoints: [
                { y: datesServed, label: "Days served since 6 Jan" },
                { y: datesRemaining, label: `Days until ${randomDay} ${randomMonth}`, exploded: true },
            ]
        }]
    });
    chart.render();
}
updateChart(new Date(2021, 2, 26));
